# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Steven Seifried <gitlab@canox.net>

pkgname=tuxedo-drivers-dkms
pkgver=4.12.2
pkgrel=1
pkgdesc="TUXEDO Computers kernel module drivers for keyboard, keyboard backlight & general hardware I/O using the SysFS interface"
arch=('x86_64')
url="https://gitlab.com/tuxedocomputers/development/packages/tuxedo-drivers"
license=('GPL-2.0-or-later')
depends=('dkms')
provides=(
  'clevo-acpi'
  'clevo-wmi'
  'ite_829x'
  'ite_8291'
  'ite_8291_lb'
  'ite_8297'
  'tuxedo-keyboard'
  'tuxedo-keyboard-ite'
  'tuxedo-io'
  'uniwill-wmi')
conflicts=('tuxedo-keyboard-dkms' 'tuxedo-keyboard-ite-dkms')
replaces=('tuxedo-keyboard-dkms' 'tuxedo-keyboard-ite-dkms')
source=("$url/-/archive/v$pkgver/${pkgname%-dkms}-v$pkgver.tar.gz")
sha256sums=('147b1fc03b6da766c3bc9fb906c59b7b0e706698a168ec62d68f36fad5c0d1ff')

package() {
  cd "${pkgname%-dkms}-v$pkgver"
  install -Dm644 Makefile -t "$pkgdir/usr/src/${pkgname%-dkms}-$pkgver/"
  install -m644 debian/tuxedo-drivers.dkms "$pkgdir/usr/src/${pkgname%-dkms}-$pkgver/dkms.conf"
  sed -i "s/#MODULE_VERSION#/$pkgver/g" "$pkgdir/usr/src/${pkgname%-dkms}-$pkgver/dkms.conf"

  install -Dm644 tuxedo_keyboard.conf -t "$pkgdir/usr/lib/modprobe.d/"
  install -Dm644 99-z-tuxedo-systemd-fix.rules -t "$pkgdir/usr/lib/udev/rules.d/"
  install -Dm644 99-infinityflex-touchpanel-toggle.rules  -t "$pkgdir/etc/udev/rules.d/"
  install -Dm644 61-sensor-infinityflex.hwdb -t "$pkgdir/usr/lib/udev/hwdb.d/"

  cp -r src/* "$pkgdir/usr/src/${pkgname%-dkms}-$pkgver/"
}
